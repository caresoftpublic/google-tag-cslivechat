<script>
  document.addEventListener("cs_widgetTracking",function(e){
        console.log(e);
        switch (e.action) {
            case "agent_join_conversation":
                console.log("agent_join_conversation");
                window.dataLayer.push({
                    "event": "csChat",
                    "csChatAction": "agent_join_conversation"
                });
                break;
            case "start_chat":
                console.log("start_chat");
                window.dataLayer.push({
                    "event": "csChat",
                    "csChatAction": "start_chat"
                });
                break;
            case "open_chat":
                console.log("open_chat");
                window.dataLayer.push({
                    "event": "csChat",
                    "csChatAction": "open_chat"
                });
                break;
            case "offline_message_sent":
                console.log("offline_message_sent");
                window.dataLayer.push({
                    "event": "csChat",
                    "csChatAction": "offline_message_sent"
                });
                break;
            case "rating_good":
                console.log("rating_good");
                window.dataLayer.push({
                    "event": "csChat",
                    "csChatAction": "rating_good"
                });
                break;
            case "rating_bad":
                console.log("rating_bad");
                window.dataLayer.push({
                    "event": "csChat",
                    "csChatAction": "rating_bad"
                });
                break;
            case "remove_rating":
                console.log("remove_rating");
                window.dataLayer.push({
                    "event": "csChat",
                    "csChatAction": "remove_rating"
                });
                break;
            case "agent_start_chat":
                console.log("agent_start_chat");
                window.dataLayer.push({
                    "event": "csChat",
                    "csChatAction": "agent_start_chat"
                });
                break;
          default:
              console.log(e.action);
                window.dataLayer.push({
                    "event": "csChat",
                    "csChatAction": e.action
                });
                break;
        }
    },false);

</script>
